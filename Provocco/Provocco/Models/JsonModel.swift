//
//  JsonModel.swift
//  Adrian Zghibarta Utils
//
//  Created by Adrian Zghibarta on 1/14/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import UIKit

/// Basic JsonModel for web requests
@objc
protocol JsonModel {
    
    /**
     Instantiate the object using the values from the json dictionary
     
     - parameter dictionary: the dictionary with the object information
     
     - returns: object instance
     */
    init(jsonDictionary: NSDictionary)
}
