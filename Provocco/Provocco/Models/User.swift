//
//  User.swift
//  Provocco
//
//  Created by Adrian Zghibarta on 4/2/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit

class User: NSObject, JsonModel {
    
    //
    //
    // -------------------------------------------------------------------------------
    // MARK: - Mappings
    
    enum MappingKeys: String {
        case idKey = "id"
        case nameKey = "name"
        case emailKey = "email"
        case ageKey = "age"
        case locationKey = "location"
        case fbTokenKey = "fbToken"
        case pictureUrlKey = "pictureUrl"
        case newUser = "newUser"
    }
    
    //
    //
    // -------------------------------------------------------------------------------
    // MARK: - Proprietes
    
    var id: Int?
    var name: String?
    var email: String?
    var age: Int?
    var location: String?
    var fbToken: String?
    var pictureUrl: NSURL?
    var newUser: Bool = false
    
    //
    //
    // -------------------------------------------------------------------------------
    // MARK: - Initialisation
    
    override init() {
         super.init()
    }
    
    required init(jsonDictionary: NSDictionary) {
        
        self.id = jsonDictionary[MappingKeys.idKey.rawValue] as? Int
        self.name = jsonDictionary[MappingKeys.nameKey.rawValue] as? String
        self.email = jsonDictionary[MappingKeys.emailKey.rawValue] as? String
        self.age = jsonDictionary[MappingKeys.ageKey.rawValue] as? Int
        self.location = jsonDictionary[MappingKeys.idKey.rawValue] as? String
        
        if let url = jsonDictionary[MappingKeys.pictureUrlKey.rawValue] as? String {
            self.pictureUrl = NSURL(string: url)
        }
        
        self.newUser = jsonDictionary[MappingKeys.newUser.rawValue] as? Bool ?? false
    }
}
