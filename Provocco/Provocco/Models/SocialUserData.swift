//
//  SocialUserData.swift
//  BrushnBarber
//
//  Created by Adrian Zghibarta on 3/29/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import UIKit

class SocialUserData: NSObject {
    
    enum SocialType: String {
        case Facebook = "facebook"
        case Linkedin = "linkedin"
    }
    
    var id: String?
    var email: String?
    var token: String?
    var firstName: String?
    var lastName: String?
    var birthday: NSDate?
    var pictureUrl: String?
    var location: String?
    var socialType = SocialType.Facebook
    
    // Get the validation error (id, email or token is nil - validation will be performed in this order)
    var validationError: NSError? {
        get {
            if self.id == nil {
                return NSError.errorWithLocalizedDescription(localizedDescription: "Missing Social Id")
            }
            if self.email == nil {
                return NSError.errorWithLocalizedDescription(localizedDescription: "Missing Social Email")
            }
            if self.token == nil {
                return NSError.errorWithLocalizedDescription(localizedDescription: "Missing Social Token")
            }
            if self.firstName == nil || self.lastName == nil {
                return NSError.errorWithLocalizedDescription(localizedDescription: "Missing Name Information")
            }
            if self.pictureUrl == nil {
                return NSError.errorWithLocalizedDescription(localizedDescription: "Missing Picture")
            }
            return nil
        }
    }
    
    var name: String {
        get {
            var stringToReturn = ""
            if let firstNameSafe = self.firstName {
                stringToReturn.appendContentsOf(firstNameSafe + " ")
            }
            if let lastNameSafe = self.lastName {
                stringToReturn.appendContentsOf(lastNameSafe)
            }
            
            return stringToReturn
        }
    }
    
    var age: Int {
        get {
            if let date = self.birthday {
                
                //Here I’m creating the calendar instance that we will operate with:
                let calendar = NSCalendar.init(calendarIdentifier: NSCalendarIdentifierGregorian)
                let currentYearInt = (calendar?.component(NSCalendarUnit.Year, fromDate: NSDate()))!
                let birthdayYearInt = (calendar?.component(NSCalendarUnit.Year, fromDate: date))!
                return currentYearInt - birthdayYearInt
            }
            else {
                return 0
            }
        }
    }
}

