//
//  NavigationController.swift
//  LeJet
//
//  Created by Adrian Zghibarta on 2/8/16.
//  Copyright © 2016 Soluti. All rights reserved.
//

import Foundation
import UIKit

class NavigationController: UINavigationController, UINavigationControllerDelegate, UINavigationBarDelegate {
    
    /// The handler for the back button of the first view controller (this disable the reveal left menu action)
    var backHandler: (() -> Void)? {
        didSet {
            if self.viewControllers.count > 0 {
                let firstVC = self.viewControllers[0]
                self.setCustomBackButtonWithCustomHandlerForViewController(firstVC)
            }
        }
    }
    
    override func viewDidLoad() {
        // Customize the navigation bar appeareance
        
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationBar.barTintColor = UIColor.blueThemeColor
        self.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont.fontWithName(FontNames.PTSans_Bold, size: 16)!,
            NSForegroundColorAttributeName: UIColor.whiteColor()]
    }
    
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    func navigationBar(navigationBar: UINavigationBar, shouldPushItem item: UINavigationItem) -> Bool {
        // Getting the numbers of view controllers from the stack
        let vcCount = self.viewControllers.count
        
        // Get the current view controller
        let currentVC = self.viewControllers[vcCount - 1]
        
        // If the view controller isn't the first then set the custom back button item
        if vcCount > 1 {
            
            self.setCustomBackButtonForViewController(currentVC)
        }
        
        self.setRightMenuButtonForViewController(currentVC)
        
        return true
    }
    
    private func setCustomBackButtonForViewController(viewController: UIViewController) {
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "BackIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: "popViewControllerAnimated:")
        viewController.navigationItem.leftBarButtonItem = leftButton
    }
    
    private func setRightMenuButtonForViewController(viewController: UIViewController) {
        let rightButton = UIBarButtonItem(image: UIImage(named: "MenuIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: "presentMenuViewController")
        viewController.navigationItem.rightBarButtonItem = rightButton
    }
    
    func presentMenuViewController() {
        print("PresentMenuViewController")
        
        let menu = MenuViewController.loadFromNib()
        menu.presenterViewController = self
        self.presentViewController(menu, animated: true, completion: nil)
    }
    
    private func setCustomBackButtonWithCustomHandlerForViewController(viewController: UIViewController) {
        let button = UIButton(type: .Custom)
        button.frame = CGRectMake(0, 0, 20, 20)
        button.setImage(UIImage(named: "BackIcon"), forState: UIControlState.Normal)
        button.addTarget(self, action: "callBackHandler", forControlEvents: UIControlEvents.TouchUpInside)
        
        let barButtonItem = UIBarButtonItem(customView: button)
        viewController.navigationItem.leftBarButtonItem = barButtonItem
    }
    
    /**
     Call the back action handler
     */
    func callBackHandler() {
        
        if let handler = self.backHandler {
            handler()
        }
    }
    
    /**
     Make the navigation bar visible with transparent background
     */
    func presentTransparentNavigationBar() {
        navigationBar.translucent = true
        navigationBar.setBackgroundImage(UIImage(), forBarMetrics:UIBarMetrics.Default)
        navigationBar.shadowImage = UIImage()
        setNavigationBarHidden(false, animated:true)
    }
    
    /**
     Hide the navigation bar
     */
    func hideTransparentNavigationBar() {
        setNavigationBarHidden(true, animated:false)
        navigationBar.setBackgroundImage(UINavigationBar.appearance().backgroundImageForBarMetrics(UIBarMetrics.Default), forBarMetrics:UIBarMetrics.Default)
        navigationBar.translucent = UINavigationBar.appearance().translucent
        navigationBar.shadowImage = UINavigationBar.appearance().shadowImage
    }
}
