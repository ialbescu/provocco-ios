//
//  ThemeEmptyButton.swift
//  Provocco
//
//  Created by Adrian Zghibarta on 4/2/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit

class ThemeEmptyButton: UIButton {

    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        self.layer.cornerRadius = 3.0
        self.layer.masksToBounds = true
        self.backgroundColor = UIColor.whiteColor()
        self.setTitleColor(UIColor.blueThemeColor, forState: .Normal)
        
        self.layer.borderColor = UIColor.blueThemeColor?.CGColor
        self.layer.borderWidth = 1.0
    }

}
