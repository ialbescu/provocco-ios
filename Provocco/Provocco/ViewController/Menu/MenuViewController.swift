//
//  MenuViewController.swift
//  Provocco
//
//  Created by Adrian Zghibarta on 4/2/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    //
    //
    // -------------------------------------------------------------------------------
    // MARK: - Proprietes
    
    var presenterViewController: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    class func loadFromNib() -> MenuViewController {
        let viewController = MenuViewController(nibName: String(MenuViewController), bundle: nil)
        
        return viewController
    }
    
    //
    //
    // -------------------------------------------------------------------------------
    // MARK: - Actions
    
    
    @IBAction private func closeAction(sender: UIButton?) {
        
        self.presenterViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction private func showProfile(sender: UIButton?) {
        
        print("Show Profile")
        self.presentAlertViewController(alertTitle: "Houston, we have a problem: This isn't implemented", alertMessage: "Please try to act like nothing happened...")
    }
    
    @IBAction private func logoutAction(sender: UIButton?) {
        
        NavigationManager.sharedInstance.setRootViewController(LoginViewController.loadFromNib())
    }
    
    @IBAction private func showCreatedChalanges(sender: UIButton?) {
        
        self.presentAlertViewController(alertTitle: "Houston, we have a problem: This isn't implemented", alertMessage: "Please try to act like nothing happened...")
    }
    
    @IBAction private func showJoinedChallanges(sender: UIButton?) {
        
        self.presentAlertViewController(alertTitle: "Houston, we have a problem: This isn't implemented", alertMessage: "Please try to act like nothing happened...")
    }
    
    @IBAction private func showCompletedChallanges(sender: UIButton?) {
        
        self.presentAlertViewController(alertTitle: "Houston, we have a problem: This isn't implemented", alertMessage: "Please try to act like nothing happened...")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
