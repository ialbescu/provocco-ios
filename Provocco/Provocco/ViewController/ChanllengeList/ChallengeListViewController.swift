//
//  ChallengeViewController.swift
//  Provocco
//
//  Created by Alexandru Bereghici on 4/2/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit

class ChallengeListViewController: UIViewController {

    //---------------------------------------------------------------
    //MARK: Constants
    
    let kFilterButtonTitle = "Filter by"
    
    //---------------------------------------------------------------
    //MARK: IBOutlet
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var filterButton: UIButton!
    @IBOutlet var noDataLabel: UILabel!
    
    //---------------------------------------------------------------
    //MARK: Proprietes
    
    var filtersController = FiltersViewController.loadFromNib()
    
    var challengesContainer = [Challenge]()
    
    var refreshControl = UIRefreshControl()
    
    //---------------------------------------------------------------
    //MARK: Main Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setting the  UITableView
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100.0
        self.tableView.registerNib(ChallengeListViewCell.nib(), forCellReuseIdentifier: String(ChallengeListViewCell))
        
        //Add pull to refresh controller
        self.refreshControl.addTarget(self, action: Selector("getAllAppointments"), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl)
        
        //Setting delegate for filters
        self.filtersController.delegate = self
        
        self.title = "Challanges"
        
        self.noDataLabel.hidden = false
        
        self.getAllAppointments()
    }
    
    //---------------------------------------------------------------
    //MARK: Requests
    
    func getAllAppointments() {
        
        ChallengeManager.sharedInstance.getAllChallenges(succesHandler: { (challenges, originalResult) in
            
            self.challengesContainer = challenges
            self.noDataLabel.hidden = true
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        
            }) { (error) in
            
               self.tableView.hidden = false
                self.refreshControl.endRefreshing()
        }
    }
    
    
    class func loadFromNib() -> ChallengeListViewController {
        let viewController = ChallengeListViewController(nibName: String(ChallengeListViewController), bundle: nil)
        
        return viewController
    }
    
    func displayFiltersViewController(animationType: SLpopupViewAnimationType) {
        
        self.presentpopupViewController(self.filtersController, animationType: animationType, completion: { () -> Void in
        
        })
    }
    
    //---------------------------------------------------------------
    //MARK: Action Buttons
    
    @IBAction func didPressFilterButton(sender: AnyObject) {
        
        self.displayFiltersViewController(.BottomTop)
    }
    
    @IBAction func didPressMapButton(sender: AnyObject) {
        let mvc = MapViewController()
        mvc.challenges = self.challengesContainer
        mvc.editable = false
        self.navigationController?.pushViewController(mvc, animated: true)
    }
    
    @IBAction func didPressCreateButton(sender: AnyObject) {
        
        let createVC = CreateChallangeViewController.loadFromNib()
        self.navigationController?.pushViewController(createVC, animated: true)
    }
}

//---------------------------------------------------------------
//MARK: UITableViewDelegate, UITableViewDataSource

extension ChallengeListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.challengesContainer.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(String(ChallengeListViewCell)) as! ChallengeListViewCell
        
        cell.configureCellWithChallenge(self.challengesContainer[indexPath.row])
        
        return cell
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let previewChallenge = PreviewChallengeViewController.loadFromNib()
        previewChallenge.selectedChallenge = self.challengesContainer[indexPath.row]
        
        self.navigationController?.pushViewController(previewChallenge, animated: true)
    }
}

//---------------------------------------------------------------
// MARK: - AddAccountViewControllerDelegate

extension ChallengeListViewController: FiltersViewControllerDelegate {
    
    /**
     *  Here we declared two delegates which will be executed when user Tap OK or Cancel Button
     in FiltersViewController
     */
    
    func pressOK(sender: FiltersViewController, filter: FilterBy) {
        
        if filter == FilterBy.None {
             self.filterButton.setTitle(kFilterButtonTitle, forState: .Normal)
        }
        else {
            let title = self.kFilterButtonTitle + ": " + String(filter)
            self.filterButton.setTitle(title, forState: .Normal)
        }
        self.dismissPopupViewController(.Fade)
    }
    
    func pressCancel(sender: FiltersViewController) {
        self.dismissPopupViewController(.Fade)
    }
}
