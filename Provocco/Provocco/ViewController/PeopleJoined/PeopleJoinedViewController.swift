//
//  PeopleJoinedViewController.swift
//  Provocco
//
//  Created by Alexandru Bereghici on 4/2/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit

class PeopleJoinedViewController: UIViewController {

    //---------------------------------------------------------------
    //MARK: Constants
    
    let kFilterButtonTitle = "Filter by"
    
    //---------------------------------------------------------------
    //MARK: IBOutlet
    
    @IBOutlet var tableView: UITableView!
    
    //---------------------------------------------------------------
    //MARK: Proprietes
    
    var filtersController = FiltersViewController.loadFromNib()
    
    //---------------------------------------------------------------
    //MARK: Main Functions
    
    class func loadFromNib() -> PeopleJoinedViewController {
        let viewController = PeopleJoinedViewController(nibName: String(PeopleJoinedViewController), bundle: nil)
        
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setting the  UITableView
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 60.0
        self.tableView.registerNib(PeopleJoinedCell.nib(), forCellReuseIdentifier: String(PeopleJoinedCell))
    }
    
    //---------------------------------------------------------------
    //MARK: Action Buttons
}

//---------------------------------------------------------------
//MARK: UITableViewDelegate, UITableViewDataSource

extension PeopleJoinedViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(String(PeopleJoinedCell)) as! PeopleJoinedCell
        
        return cell
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let profileController = ProfileInfoViewController.loadFromNib()
        
        self.navigationController?.pushViewController(profileController, animated: true)
        
        //self.presentViewController(profileController, animated: true, completion: nil)
    }
}
