//
//  ProfileInfoViewController.swift
//  Provocco
//
//  Created by Alexandru Bereghici on 4/2/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit

class ProfileInfoViewController: UIViewController {

    //---------------------------------------------------------------
    //MARK: Constants
    
    //---------------------------------------------------------------
    //MARK: IBOutlet
    
    @IBOutlet var photoImageView: UIImageView! {
        didSet {
            self.photoImageView.layer.cornerRadius = self.photoImageView.frame.size.width / 2
            self.photoImageView.clipsToBounds = true
        }
    }

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var joinedChallengesLabel: UILabel!
    @IBOutlet var completedChallengesLabel: UILabel!
    @IBOutlet var createdChallengesLabel: UILabel!
    
    //---------------------------------------------------------------
    //MARK: Variables
    
    //---------------------------------------------------------------
    //MARK: Main Functions

    class func loadFromNib() -> ProfileInfoViewController {
        let viewController = ProfileInfoViewController(nibName: String(ProfileInfoViewController), bundle: nil)
        
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
}
