//
//  TutorialViewController.swift
//  Provocco
//
//  Created by Adrian Zghibarta on 4/3/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var pageViewController: UIPageViewController!
    
    var viewControllers = [UIViewController]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initializePageViewController()
        self.initializePages()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    class func loadFromNib() -> TutorialViewController {
        let viewController = TutorialViewController(nibName: String(TutorialViewController), bundle: nil)
        
        return viewController
    }
    
    /**
     Initialize the UIPageViewController and add it to the self.view with constraints
     */
    private func initializePageViewController() {
        //Initialize the pageViewConroller
        self.pageViewController = UIPageViewController(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)
        self.pageViewController.view.backgroundColor = UIColor.clearColor()
        self.pageViewController.delegate = self
        self.pageViewController.dataSource = self
        
        //Add the pageViewController to the superView
        self.addChildViewController(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.didMoveToParentViewController(self)
        
        //Set the constraints
        self.setConstraintsForPageViewController()
    }
    
    /**
     Set the constraints for the self.pageViewController (center and equal dimensions with self.containerView)
     */
    private func setConstraintsForPageViewController() {
        self.pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        
        let centerX: NSLayoutConstraint = NSLayoutConstraint(item: self.pageViewController.view, attribute: .CenterX, relatedBy: .Equal, toItem: self.containerView, attribute: .CenterX, multiplier: 1, constant: 0)
        
        let centerY: NSLayoutConstraint = NSLayoutConstraint(item: self.pageViewController.view, attribute: .CenterY, relatedBy: .Equal, toItem: self.containerView, attribute: .CenterY, multiplier: 1, constant: 0)
        
        let equalWidth: NSLayoutConstraint = NSLayoutConstraint(item: self.pageViewController.view, attribute: .Width, relatedBy: .Equal, toItem: self.containerView, attribute: .Width, multiplier: 1, constant: 0)
        
        let equalHeight: NSLayoutConstraint = NSLayoutConstraint(item: self.pageViewController.view, attribute: .Height, relatedBy: .Equal, toItem: self.containerView, attribute: .Height, multiplier: 1, constant: 0)
        
        self.view.addConstraints([centerX, centerY, equalHeight, equalWidth])
    }
    
    
    /**
     Initialize the array of view controller
     */
    private func initializePages() {
        
        let firstutorial = TutorialPageViewController.loadFromNib()
        firstutorial.titleString = "This is a tutorial screen..."
        firstutorial.logoImage = UIImage(named: "Tutorial1")
        
        let secontutorial = TutorialPageViewController.loadFromNib()
        secontutorial.titleString = "This is another totorial screen..."
        secontutorial.logoImage = UIImage(named: "Tutorial2")
        
        let onemoretutorial = TutorialPageViewController.loadFromNib()
        onemoretutorial.titleString = "Guess what..."
        onemoretutorial.logoImage = UIImage(named: "Tutorial3")
        
        let lasttutorial = TutorialPageViewController.loadFromNib()
        lasttutorial.titleString = "This is the best tutorial ever..."
        lasttutorial.logoImage = UIImage(named: "Tutorial4")
        
        self.viewControllers.appendContentsOf([firstutorial, secontutorial, onemoretutorial, lasttutorial])
        
        self.pageViewController.setViewControllers([self.viewControllers[0]], direction: .Forward, animated: false, completion: nil)
        
    }
    
    @IBAction private func doneAction(sender: UIButton?) {
        
        let challengeListViewController = ChallengeListViewController.loadFromNib()
        let navigationController = NavigationController(rootViewController: challengeListViewController)
        navigationController.modalTransitionStyle = .FlipHorizontal
        self.presentViewController(navigationController, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TutorialViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        if var indexValue = self.viewControllers.indexOf(viewController) {
            indexValue += 1
            if indexValue == self.viewControllers.count {
                return nil
            }
            else {
                return self.viewControllers[indexValue]
            }
        }
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        if var indexValue = self.viewControllers.indexOf(viewController) {
            if indexValue == 0 {
                return nil
            }
            indexValue -= 1
            return self.viewControllers[indexValue]
        }
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        let actualViewController = pageViewController.viewControllers![0]
        if let indexValue = self.viewControllers.indexOf(actualViewController) {
            self.pageControl.currentPage = indexValue
        }
        
    }
}
