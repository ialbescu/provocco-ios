//
//  PreviewChallengeViewController.swift
//  Provocco
//
//  Created by Bogdan Cata on 02/04/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit

class PreviewChallengeViewController: UIViewController {
    
    
    //---------------------------------------------------------------
    //MARK: Constants
    
    //---------------------------------------------------------------
    //MARK: IBOutlet
    
    @IBOutlet var titleChallenge: UILabel!
    @IBOutlet var joined: UILabel!
    @IBOutlet var completed: UILabel!
    @IBOutlet var created: UILabel!
    
    @IBOutlet var photoImageView: UIImageView! {
        didSet {
            self.photoImageView.layer.cornerRadius = self.photoImageView.frame.size.width / 2
            self.photoImageView.clipsToBounds = true
        }
    }

    //---------------------------------------------------------------
    //MARK: Proprietes
    
    var selectedChallenge: Challenge?
    
    //---------------------------------------------------------------
    //MARK: Main Functions

    class func loadFromNib() -> PreviewChallengeViewController {
        let viewController = PreviewChallengeViewController(nibName: String(PreviewChallengeViewController), bundle: nil)
        
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let challenge = self.selectedChallenge {
            
            self.titleChallenge.text = challenge.title
            self.joined.text = "10"//String(challenge.peopleJoined.count)
            self.completed.text = "11"//String(challenge.peopleCompleted.count)
            self.created.text = "1"//String(challenge.peopleAproved.count)
        }
    }
    
    //---------------------------------------------------------------
    //MARK: IBActions
    
    @IBAction func didPressPeopleButton() {
        
        let profileInfo = PeopleJoinedViewController.loadFromNib()
        
        self.navigationController?.pushViewController(profileInfo, animated: true)
    }
    
    @IBAction func didPressMapButton() {
        
        let mapView = MapViewController.loadFromNib()
        
        self.navigationController?.pushViewController(mapView, animated: true)
    }
}
