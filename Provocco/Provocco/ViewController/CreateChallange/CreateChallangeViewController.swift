//
//  CreateChallangeViewController.swift
//  Provocco
//
//  Created by Bogdan Cata on 02/04/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit


enum MediaType: Int {
    
    case Photo  = 0
    case Video  = 1
    case Audio  = 2
    case Text   = 3
}

enum ChallengeType: Int {
    
    case Private     = 0
    case Public      = 1
    case Charity     = 2
}


class CreateChallangeViewController: UIViewController {

    //MARK Variables
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleTextField: UITextField! {
        didSet {
            titleTextField.layer.borderColor = UIColor.blueThemeColor?.CGColor
            descriptionBackView.layer.borderWidth = 1.0
        }
    }
    @IBOutlet weak var descriptionTextView: UITextView! {
        didSet {
            descriptionTextView.layer.borderColor = UIColor.blueThemeColor?.CGColor
            descriptionTextView.layer.cornerRadius = 0.3
            descriptionTextView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var descriptionBackView: UIView! {
        didSet {
            descriptionBackView.layer.borderColor = UIColor.blueThemeColor?.CGColor
            descriptionBackView.layer.borderWidth = 1.0
            descriptionBackView.layer.cornerRadius = 3.0
            descriptionBackView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var textfieldBackground: UIView! {
        didSet {
            textfieldBackground.layer.borderColor = UIColor.blueThemeColor?.CGColor
            textfieldBackground.layer.borderWidth = 1.0
            textfieldBackground.layer.cornerRadius = 3.0
            textfieldBackground.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var challengeTypeControl: UISegmentedControl!
    @IBOutlet weak var selectLocationButton: UIButton!
    @IBOutlet weak var createChallengeButton: UIButton!
    
    @IBOutlet var uploadPhotoTapGesture: UITapGestureRecognizer!
    @IBOutlet var uploadVideoTapGesture: UITapGestureRecognizer!
    @IBOutlet var uploadAudioTapGesture: UITapGestureRecognizer!
    
    
    private var challengeType = ChallengeType.Private
    // location
    private var locationLongitude: NSNumber?
    private var locationLatitude: NSNumber?
    // resource upload
    private var uploadURL: NSURL?
    private var uploadType = MediaType.Text
    
    
    //MARK Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        self.descriptionLabel!.addTarget(self, action: "didPressDescriptionLabel:", forControlEvents: .TouchUpInside)
        self.uploadPhotoTapGesture!.addTarget(self, action: "didTapUploadPhoto:")
        self.uploadVideoTapGesture!.addTarget(self, action: "didTapUploadVideo:")
        self.uploadAudioTapGesture!.addTarget(self, action: "didTapUploadAudio:")
        self.challengeTypeControl!.addTarget(self, action: "didSelectChallengeType:", forControlEvents: .TouchUpInside)
        self.selectLocationButton!.addTarget(self, action: "didPressSelectLocation:", forControlEvents: .TouchUpInside)
        self.createChallengeButton!.addTarget(self, action: "didPressCreateChallenge:", forControlEvents: .TouchUpInside)
        
        self.title = "Create Challange."
    }
    
    class func loadFromNib() -> CreateChallangeViewController {
        let viewController = CreateChallangeViewController(nibName: String(CreateChallangeViewController), bundle: nil)
        
        return viewController
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction private func hidekeyBoard(sender: UITapGestureRecognizer?) {
        self.view.endEditing(true)
    }
    
    func didPressDescriptionLabel(sender:AnyObject) {
        
        // TODO
    }
    
    func didTapUploadPhoto(sender:AnyObject) {
        
        self.uploadType = MediaType.Photo
        // TODO
    }
    
    func didTapUploadVideo(sender:AnyObject) {
        
        self.uploadType = MediaType.Video
        // TODO
    }
    
    func didTapUploadAudio(sender:AnyObject) {
        
        self.uploadType = MediaType.Audio
        // TODO
    }
    
    func didSelectChallengeType(sender:UISegmentedControl) {
        
        self.challengeType = ChallengeType(rawValue: sender.selectedSegmentIndex)!
    }

    func didPressSelectLocation(sender:AnyObject) {
        
        let mvc = MapViewController()
        mvc.editable = true
        mvc.delegate = self
        self.navigationController?.pushViewController(mvc, animated: true)

    }
    
    func didPressCreateChallenge(sender:AnyObject) {
        
        if !String.validateEmptyNotNilStrings([self.titleTextField.text, self.descriptionTextView.text]) {
            self.presentErrorAlertViewController(alertMessage: "Please give a title and description to this challange")
            return
        }
        
        let challange = Challenge()
        
        challange.authorId = 12
        challange.title = self.titleTextField.text
        challange.descr = self.descriptionTextView.text
        challange.type = "PRIVATE"
        challange.photoURL = NSURL(string: "http://www.wanderlusting.info/sites/default/files/styles/adaptive/public/travel-tips-blue-the-astor-noho-blue-man-face.jpg")
        challange.type = "PICTURE"
        challange.locationName = "Iasi"
        challange.longitude = 12.12
        challange.latitude = 12.12
        
        ChallengeManager.sharedInstance.createChallenge(
            challange,
            succesHandler: { (challenges, originalResult) in
                self.navigationController?.popViewControllerAnimated(true)
        }) { (error) in
            self.presentErrorAlertViewController(alertMessage: error.localizedDescription)
        }
        
        
    }
}

extension CreateChallangeViewController: MapViewControllerDelegate {
    
    func mapViewCoordinatesReturned(coord: CLLocationCoordinate2D) {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
