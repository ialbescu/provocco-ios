//
//  PaginationManager.swift
//  Adrian Zghibarta Utils
//
//  Created by Adrian Zghibarta on 1/15/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import UIKit

/// Basic functionality for pagination
class PaginationManager: NSObject {
    
    private var maximumNumberOfObjects = 200
    private var itemsPerPage = 10
    private var actualPage = 0
    private var isPaginationEnd = false
    /// Closure to handle the next page operations
    var nextPageHandler: ((newPageNumber: Int, actualItemsNumber: Int) -> ())?
    /// Closure to handle the end of pages
    var endPaginationHandler: (() -> ())?
    
    /**
     Initialize a Pagination Manager Structure with maximum number of pages and items per page
     
     - parameter maxNumberOfObject: Maximum number of objects ot display
     - parameter itemsPerPage:      Items per page to display
     
     - returns: PaginationManager Structure
     */
    init(maxNumberOfObject: Int, itemsPerPage: Int){
        self.maximumNumberOfObjects = maxNumberOfObject
        self.itemsPerPage = itemsPerPage
        self.actualPage = 0
    }
    
    /**
     Reset The actual Page to 0
     */
    func resetPages() {
        actualPage = 0
        isPaginationEnd = false
    }
    
    
    /**
     Process the next page events
     */
    func nextPage() {
        actualPage += 1
        if  (actualPage * itemsPerPage <= maximumNumberOfObjects) {
            if let handler = self.nextPageHandler {
                handler(newPageNumber: actualPage, actualItemsNumber: actualPage * itemsPerPage)
            }
        }
        else {
            self.isPaginationEnd = true
            if let handler = self.endPaginationHandler {
                handler()
            }
        }
    }
    
    /**
     Permets to see if the last page was displayed
     
     - returns: Bool value - if the last page was displayied
     */
    func hasFinishPagination() -> Bool {
        return self.isPaginationEnd
    }
}
