//
//  UIViewExtensions.swift
//  BrushnBarber
//
//  Created by Adrian Zghibarta on 3/30/16.
//  Copyright © 2016 Soluti. All rights reserved.
//

import UIKit
import Foundation

extension UIView {
    
    class func loadFromNib(forOwner owner: AnyObject? = nil) -> AnyObject {
        
        let className = NSStringFromClass(self).componentsSeparatedByString(".").last
        return NSBundle.mainBundle().loadNibNamed(className, owner: owner, options: nil)[0]
    }
}
