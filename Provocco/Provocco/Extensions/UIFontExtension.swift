//
//  UIFontExtension.swift
//  Adrian Zghibarta Utils
//
//  Created by Adrian Zghibarta on 2/9/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import UIKit

enum FontNames: String {
    case PTSans_Bold = "HelveticaNeue-Bold"
    case PTSans_BoldItalic = "PTSans-BoldItalic"
    case PTSans_Italic = "PTSans-Italic"
    case PTSans_Regular = "PTSans-Regular"
}

extension UIFont {
    
    /**
     Create a font with a given name from an enum defined by the developer
     
     - parameter fontName: enum for the font name
     - parameter size:     size of the font
     
     - returns: UIFont object
     */
    class func fontWithName(fontName: FontNames, size: CGFloat) -> UIFont? {
        return UIFont(name: fontName.rawValue, size: size)
    }
}
