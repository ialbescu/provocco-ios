//
//  NSDateExtension.swift
//  Adrian Zghibarta Utils
//
//  Created by Adrian Zghibarta on 1/19/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import Foundation

extension NSDate {
    
    /// Date components
    var dateComponents: NSDateComponents {
        get {
            let calendar = NSCalendar.currentCalendar()
            let dateComponents = calendar.components([.Hour, .Minute, .Day, .Month], fromDate: self)
            return dateComponents
        }
    }
    
    /// Date components for UTC time zone
    var utcDateComponents: NSDateComponents {
        get {
            let calendar = NSCalendar.currentCalendar()
            calendar.timeZone = NSTimeZone(name: "UTC")!
            let dateComponents = calendar.components([.Hour, .Minute, .Day, .Month], fromDate: self)
            return dateComponents
        }
    }
    
    /// Date string for UTC time zone with full style for date and no style for time
    var utcString: String {
        get {
            let dateFormatter = NSDateFormatter()
            dateFormatter.timeZone = NSTimeZone(name: "UTC")
            dateFormatter.dateStyle = NSDateFormatterStyle.FullStyle
            dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
            return dateFormatter.stringFromDate(self)
        }
    }
    
    
    /// Hours and minutes with two digits with 'h' separator
    var utcTimeString: String {
        get {
            let dateComponents = self.utcDateComponents
            
            let formatter = NSNumberFormatter()
            formatter.minimumIntegerDigits = 2
            
            let hours = NSNumber(integer: dateComponents.hour)
            let minutes = NSNumber(integer: dateComponents.minute)
            
            let timeAsString = "\(formatter.stringFromNumber(hours)!)h\(formatter.stringFromNumber(minutes)!)"
            return timeAsString
        }
    }
    
    
    
    /**
     Compare with equal the date ignoring the hh:mm components
     
     - parameter dateToCompare: date to compare
     
     - returns: true if dates are equals and false otherwise
     */
    func isSameDateAsDate(dateToCompare: NSDate?) -> Bool {
        
        if let date = dateToCompare {
            let selfComponents = self.dateComponents
            let dateComponents = date.dateComponents
            
            if selfComponents.year == dateComponents.year && selfComponents.month == dateComponents.month && selfComponents.day == dateComponents.day {
                return true
            }
            else {
                return false
            }
        }
        else {
            return false
        }
    }
    
    /**
     Return the short abreviation for month in franch
     
     - returns: Short abrevios of month in franch
     */
    func getMonthAsFrenchString() -> String {
        return self.getShortFrenchMonthName(self.dateComponents.month)
    }
    
    /**
     Get the day from date as string
     
     - returns: Day component as string
     */
    func getDayAsString() -> String {
        let dateComponents = self.dateComponents
        let dayAsString = "\(dateComponents.day)"
        return dayAsString
    }
    
    /**
     Get the DD:HH:MM string from date
     
     - returns: String value
     */
    func getDayHoursMinutesAsString() -> String {
        let dateComponents = self.dateComponents
        
        let formatter = NSNumberFormatter()
        formatter.minimumIntegerDigits = 2
        
        let days = NSNumber(integer: dateComponents.day)
        let hours = NSNumber(integer: dateComponents.hour)
        let minutes = NSNumber(integer: dateComponents.minute)
        //let seconds = NSNumber(integer: dateComponents.second)
        
        let timeAsString = "\(formatter.stringFromNumber(days)!):\(formatter.stringFromNumber(hours)!):\(formatter.stringFromNumber(minutes)!)"
        return timeAsString
    }
    
    /**
     Get the sort franch format for a month
     
     - parameter month: Month value
     
     - returns: Short Value of month as String
     */
    func getShortFrenchMonthName(month: Int) -> String {
        switch(month) {
        case 1:
            return "JANV"
        case 2:
            return "FEVR"
        case 3:
            return "MARS"
        case 4:
            return "AVR"
        case 5:
            return "MAI"
        case 6:
            return "JUIN"
        case 7:
            return "JUIL"
        case 8:
            return "AOUT"
        case 9:
            return "SEPT"
        case 10:
            return "OCT"
        case 11:
            return "NOV"
        case 12:
            return "DEC"
        default:
            return "N/A"
        }
    }
}