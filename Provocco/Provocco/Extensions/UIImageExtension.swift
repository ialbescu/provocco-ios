//
//  UIImageExtension.swift
//  Adrian Zghibarta Utils
//
//  Created by Adrian Zghibarta on 2/3/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import UIKit

extension UIImage {
    
    /**
     Image extensions
     
     - PNG:  PNG extension
     - JPG:  JPG extension
     - JPEG: JPEG extension
     */
    enum ImageExtensions: String {
        
        case PNG = ".png"
        case JPG = ".jpg"
        case JPEG = ".jpeg"
        case UNKNOWN = ""
    }
    
    enum ImageMimeTypes: String {
        
        case PNG = "image/png"
        case JPG = "image/jpg"
        case JPEG = "image/jpeg"
        case UNKNOWN = ""
    }
    
    /**
     Get the extension of the image from a path
     
     - parameter path: path of image
     
     - returns: extension as string
     */
    class func getExtensionFromPathInformation(path path: String) -> ImageExtensions {
        
        if path.containsString(".png") || path.containsString(".PNG"){
            return ImageExtensions.PNG
        }
        else if path.containsString(".jpg") || path.containsString(".JPG"){
            return ImageExtensions.JPG
        }
        else if path.containsString(".jpeg") || path.containsString(".JPEG") {
            return ImageExtensions.JPEG
        }
        
        return ImageExtensions.UNKNOWN
    }
    
    /**
     Get the mime type of image from the her path
     
     - parameter path: path of image
     
     - returns: mime type of the image
     */
    class func getMimeTypeFromPathInformation(path path: String) -> ImageMimeTypes {
        
        if path.containsString(".png") || path.containsString(".PNG") {
            return ImageMimeTypes.PNG
        }
        else if path.containsString(".jpg") || path.containsString(".JPG") {
            return ImageMimeTypes.JPG
        }
        else if path.containsString(".jpeg") || path.containsString(".JPEG") {
            return ImageMimeTypes.JPEG
        }
        
        return ImageMimeTypes.UNKNOWN
    }
    
    /**
     Resize an image to fill the maxWidth parameter and return the resized image
     
     - parameter image:    the image to resize
     - parameter maxWidth: max width paramter
     
     - returns: resized image
     */
    class func resizedImageIfNeeded(imageToResize image: UIImage, maxWidthValue maxWidth: CGFloat) -> UIImage {
        
        let initialImageWidth = image.size.width * image.scale
        
        if initialImageWidth < maxWidth {
            
            let initialImageHeight = image.size.height * image.scale
            let ratio = (initialImageWidth < initialImageHeight) ? initialImageWidth / initialImageHeight : initialImageHeight / initialImageWidth
            let newSize = CGSizeMake(maxWidth, maxWidth / ratio)
            
            UIGraphicsBeginImageContext(newSize)
            image.drawInRect(CGRectMake(0, 0, newSize.width, newSize.width))
            
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return resizedImage
        }
        else {
            return image
        }
    }
    
    /**
     Transform an image to NSData
     
     - parameter image: image
     - parameter ext:   image extension
     
     - returns: NSData from image
     */
    class func transfrormImageToNSData(image image: UIImage, imageExtension ext: ImageExtensions) -> NSData? {
        
        var data: NSData?
        switch(ext) {
        case .JPEG,.JPG:
            data = UIImageJPEGRepresentation(image, 0.5)
            return data
        case .PNG:
            data = UIImagePNGRepresentation(image)
            return data
        default:
            return data
        }
    }
    
    /**
     Get the mb size of the image
     
     - returns: Mb size of image
     */
    func getMegabyteSize() -> Float {
        
        if let imageData = UIImageJPEGRepresentation(self, 0.5) {
            let imageSize = Float(imageData.length)
            print("The selected image size is : \(imageSize) bytes")
            return imageSize / 1048576
        }
        
        return 0
    }
}
