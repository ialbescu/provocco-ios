//
//  UITextFieldExtension.swift
//  Adrian Zghibarta Utils
//
//  Created by Adrian Zghibarta on 1/13/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    /**
     Set the left image
     
     - parameter image: left image
     */
    func setLeftImage(image: UIImage?) {
        if let leftImage = image {
            self.leftViewMode = UITextFieldViewMode.Always
            let imageView = UIImageView(image: leftImage)
            imageView.frame = CGRectMake(5, 10, imageView.frame.size.width - 5, imageView.frame.height - 20)
            imageView.contentMode = UIViewContentMode.ScaleAspectFit
            self.leftView = imageView
        }
        else {
            self.leftViewMode = UITextFieldViewMode.Never
            self.leftView = nil
        }
    }
    
    /**
     Set a placeholder text with a color
     
     - parameter placeholderText:  placeholder text
     - parameter placeholderColor: placeholder color
     */
    func setPlaceholder(placeholderText: String, placeholderColor: UIColor) {
        self.attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSForegroundColorAttributeName: placeholderColor])
    }
    
    /**
     Validate if the text of the textField is emplty or not
     
     - returns: true if the text isn't emplty
     */
    func validateEmptyText() -> Bool {
        if let textToValidate = self.text {
            if textToValidate.isEmpty {
                return false
            }
            else {
                return true
            }
        }
        else {
            return false
        }
    }
}