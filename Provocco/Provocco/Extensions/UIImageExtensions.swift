//
//  UIImageExtensions.swift
//  Adrian Zghibarta Utils
//
//  Created by Adrian Zghibarta on 2/15/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import UIKit
import Foundation

extension UIImage {
    
    class func imageFromView(view: UIView) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return img
    }
}