//
//  StringExtension.swift
//  Adrian Zghibarta Utils
//
//  Created by Adrian Zghibarta on 2/5/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import UIKit
import Foundation

extension String {
    
    /**
     Return the class string name
     
     - parameter aClass: class
     
     - returns: class name as string
     */
    static func className(aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).componentsSeparatedByString(".").last!
    }
    
    /**
     Substring from index
     
     - parameter from: start index for the substring
     
     - returns: substring
     */
    func substring(from: Int) -> String {
        return self.substringFromIndex(self.startIndex.advancedBy(from))
    }
    
    /// The length of string - characters count
    var length: Int {
        return self.characters.count
    }
    
    /**
     Validate the text for an array of text fields
     
     - parameter textFields: text fields to be validated
     
     - returns: true if all text fields have valid text
     */
    static func validateEmptyNotNilTextForTextFields(textFields: [UITextField]) -> Bool{
        
        for textField in textFields {
            if let text = textField.text {
                
                let stringWihtoutSpaces = text.stringByReplacingOccurrencesOfString(" ", withString: "")
                if stringWihtoutSpaces.isEmpty {
                    return false
                }
            }
            else {
                return false
            }
        }
        
        return true
    }
    
    
    /**
     Validate an array of strings
     
     - parameter strings: text fields to be validated
     
     - returns: true if all strings are valid (not nill and not empty)
     */
    static func validateEmptyNotNilStrings(strings: [String?]) -> Bool{
        
        for string in strings {
            //
            
            if let text = string {
                
                let stringWihtoutSpaces = text.stringByReplacingOccurrencesOfString(" ", withString: "")
                if stringWihtoutSpaces.isEmpty {
                    return false
                }
            }
            else {
                return false
            }
        }
        
        return true
    }
}