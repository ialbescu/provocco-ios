//
//  UIViewControllerExtension.swift
//  Adrian Zghibarta Utils
//
//  Created by Adrian Zghibarta on 1/12/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import Foundation
import UIKit

// UIViewController Extension
extension UIViewController {
    
    /**
     Present a AlertViewController
     
     - parameter title:   title of alertViewController
     - parameter message: message of alertViewController
     */
    func presentAlertViewController(alertTitle title: String, alertMessage message: String) {
        
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel) { (alert) -> Void in
            // Implement if there is a need
        }
        alertViewController.addAction(okAction)
        self.presentViewController(alertViewController, animated: true, completion: nil)
    }
    
    /**
     Present a AlertViewController for an error
     
     - parameter message: message of alertViewController
     */
    func presentErrorAlertViewController(alertMessage message: String) {
        
        let alertViewController = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: message, preferredStyle: .Alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel) { (alert) -> Void in
            // Implement if there is a need
        }
        alertViewController.addAction(okAction)
        self.presentViewController(alertViewController, animated: true, completion: nil)
    }
    
    /**
     Set the title view as a UIImageView with the logo of the app
     */
    func setTitleAsImageView() {
        
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "AppLogo"))
    }
}
