//
//  FacebookManager.swift
//  Adrian Zghibarta
//
//  Created by Adrian Zghibarta on 3/29/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class FacebookManager: NSObject {
    
    // -------------------------------------------------------------------------------
    // MARK: - Facebook requests to get email, id and other information
    
    /**
     Get all facebook user data
     
     - parameter viewController: ViewController used to show the facebook authentificaton popup
     - parameter succesHandler:  Succes Handler
     - parameter failureHandler: Failure Handler
     - parameter cancelHandler:  Cancelation Handler
     */
    class func getFacebookUserData(
        forViewController viewController: UIViewController,
                          succesHandler: (socialUserData: SocialUserData) -> Void,
                          failureHandler: (error: NSError) -> Void,
                          cancelHandler: () -> Void)
    {
        let loginManager = FBSDKLoginManager()
        // Log out if there exist some previous connections
        loginManager.logOut()
        // Log in to facebook account
        loginManager.logInWithReadPermissions(["public_profile", "email", "user_birthday", "user_location"], fromViewController: viewController) { (result, error) -> Void in
            
            if (error != nil) {
                failureHandler(error: error)
            }
            else if result.isCancelled {
                cancelHandler()
            }
            else {
                // Facebook Token
                let fbToken = result.token.tokenString
                
                // Graph request to take the id and the email of the user
                let graphRequest = FBSDKGraphRequest(graphPath: "/me?fields=id,gender,email,first_name,last_name,birthday,location", parameters: nil)
                graphRequest.startWithCompletionHandler({ (connection, response, error) -> Void in
                    
                    // In case of facebook error
                    if let fbError = error {
                        failureHandler(error: fbError)
                    }
                        // Succes response
                    else if let fbResponse = response {
                        
                        let fbId = fbResponse["id"] as? String
                        let fbEmail = fbResponse["email"] as? String
                        let fbfirstName = fbResponse["first_name"] as? String
                        let fblastName = fbResponse["last_name"] as? String
                        var fbtown: String?
                        if let fblocation = fbResponse["location"] as? NSDictionary {
                            fbtown = fblocation["name"] as? String
                        }
                        
                        var bdDate: NSDate?
                        if let fbBirthday = fbResponse["birthday"] as? String {
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "MM/dd/yyyy"
                            bdDate = dateFormatter.dateFromString(fbBirthday)
                        }
                        
                        var fbpictureUrl: String?
                        if let fbidSafe = fbId {
                            fbpictureUrl = "https://graph.facebook.com/\(fbidSafe)/picture?type=large"
                        }
                        else {
                            fbpictureUrl = ""
                        }
                        
                        // Create the SocialUserData Object and handle the succes
                        let socialUser = SocialUserData()
                        socialUser.id = fbId
                        socialUser.firstName = fbfirstName
                        socialUser.lastName = fblastName
                        socialUser.pictureUrl = fbpictureUrl
                        socialUser.email = fbEmail
                        socialUser.token = fbToken
                        socialUser.location = fbtown
                        socialUser.birthday = bdDate
                        socialUser.socialType = .Facebook
                        
                        succesHandler(socialUserData: socialUser)
                    }
                    else {
                        failureHandler(error: NSError.errorWithLocalizedDescription(localizedDescription: NSLocalizedString("InvalidResponseFacebook", comment: "")))
                    }
                })
            }
        }
    }
}
