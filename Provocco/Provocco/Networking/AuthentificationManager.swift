//
//  AuthentificationManager.swift
//  Adrian Zghibarta
//
//  Created by Adrian Zghibarta on 3/29/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKLoginKit

class AuthentificationManager: Networking {
    
    /// Shared instance for AuthentificationManager class
    static let sharedInstance = AuthentificationManager()
    
    override init() {
        super.init()
    }
    
    //
    //
    // -------------------------------------------------------------------------------
    // MARK: - LogIn requests
    
    /**
     Log in a facebook user (the function will get all facebook user information)
     
     - parameter viewController: ViewController used to show the facebook authentifcation popup
     - parameter succesHandler:  Succes Handler
     - parameter failureHandler: Failure Handler
     - parameter cancelHandler:  Cancelation Handler
     */
    func loginUserWithFacebook(
        forViewController viewController: UIViewController,
                          succesHandler: (user: User, originalResult: AnyObject) -> Void,
                          failureHandler: (error: NSError) -> Void,
                          cancelHandler: () -> Void)
    {
        FacebookManager.getFacebookUserData(
            forViewController: viewController,
            succesHandler: { (socialUserData) -> () in
                
                self.loginFacebookAccount(
                    withSocialData: socialUserData,
                    succesHandler: succesHandler,
                    failureHandler: failureHandler)
            },
            failureHandler: { (error) -> () in
                
                failureHandler(error: error)
            },
            cancelHandler: {
                
                cancelHandler()
        })
    }
    
    private func loginFacebookAccount(
        withSocialData socialUser: SocialUserData,
                       succesHandler succesClosure: (user :User, originalResult: AnyObject) -> Void,
                                      failureHandler failureClosure: (error: NSError) -> Void)
    {
        if let userError = socialUser.validationError {
            failureClosure(error: userError)
            return
        }
        
        var parameters = [String: AnyObject]()
        
        parameters["name"] = socialUser.name
        parameters["email"] = socialUser.email
        parameters["age"] = "\(socialUser.age)"
        parameters["location"] = socialUser.location ?? "N/A"
        parameters["fbToken"] = socialUser.token
        parameters["pictureUrl"] = socialUser.pictureUrl
        
        print("Age : \(socialUser.age)")
        self.manager.request(Method.POST, NetworkingURLS.LOGIN_URL, parameters: parameters, encoding: ParameterEncoding.JSON, headers: nil)
            .response { (request, response, data, error) in
                
                print("Information about request:")
                print("Request:\n \(request)")
                print("Response:\n \(response)")
                print("Error:\n \(error)")
            }
            .responseJSON { (response) -> Void in
                
                // Get a valid json result
                if let validJsonResult = ResponseValidator.getValidJsonResult(response, failureHandler: failureClosure) {
                    
                    print(validJsonResult)
                    if let userJson = validJsonResult as? NSDictionary {
                        
                        let user = User(jsonDictionary: userJson)
                        succesClosure(user: user,originalResult: validJsonResult)
                    }
                    else {
                        failureClosure(error: NSError.errorWithLocalizedDescription(localizedDescription: NSLocalizedString("InvalidResponse", comment: "")))
                    }
                }
        }
    }
}
