//
//  ChallengeManager.swift
//  Provocco
//
//  Created by Alexandru Bereghici on 4/2/16.
//  Copyright © 2016 pentalog. All rights reserved.
//

import UIKit
import Alamofire

class ChallengeManager: Networking {
    
    /// Shared instance for ChallengeManagerManager class
    static let sharedInstance = ChallengeManager()
    
    override init() {
        super.init()
    }
    
    func getAllChallenges(succesHandler succesClosure: (challenges :[Challenge], originalResult: AnyObject) -> Void,
                                        failureHandler failureClosure: (error: NSError) -> Void)
        
    {
        
        Alamofire.request(.GET, NetworkingURLS.ALL_CHALLENGES_URL, parameters: nil)
            .response { (request, response, data, error) in
                
                print("Information about request:")
                print("Request:\n \(request)")
                print("Response:\n \(response)")
                print("Error:\n \(error)")
            }
            .responseJSON { (response) -> Void in
                
                // Get a valid json result
                if let validJsonResult = ResponseValidator.getValidJsonResult(response, failureHandler: failureClosure) {
                    
                    if let challengesJson = validJsonResult as? [NSDictionary] {
                        
                        let response = Challenge.getAllChallengesFromArray(challengesJson)
                        succesClosure(challenges: response, originalResult: validJsonResult)
                        
                    }
                    else {
                        failureClosure(error: NSError.errorWithLocalizedDescription(localizedDescription: NSLocalizedString("InvalidResponse", comment: "")))
                    }
                }
        }
    }
    
    func createChallenge(challenge: Challenge, succesHandler succesClosure: (challenges :Challenge, originalResult: AnyObject) -> Void,
                                        failureHandler failureClosure: (error: NSError) -> Void)
        
    {
        
        let parameters = [
            "authorId":String(challenge.authorId!),
            "title":challenge.title!,
            "description":challenge.descr!,
            "type":"0",//challenge.type!,
            "photo":"http://www.wanderlusting.info/sites/default/files/styles/adaptive/public/travel-tips-blue-the-astor-noho-blue-man-face.jpg",
            "uploadType":"0",
            "locationName":challenge.locationName!,
            "longitude":String(challenge.longitude!),
            "latitude":String(challenge.latitude!)
        ]
        
        self.manager.request(Method.POST, NetworkingURLS.CREATE_CHALLENGE, parameters: parameters, encoding: ParameterEncoding.JSON, headers: nil)
            .response { (request, response, data, error) in
                
                print("Information about request:")
                print("Request:\n \(request)")
                print("Response:\n \(response)")
                print("Error:\n \(error)")
            }
            .responseJSON { (response) -> Void in
                
                // Get a valid json result
                if let validJsonResult = ResponseValidator.getValidJsonResult(response, failureHandler: failureClosure) {
                    
                    if let challengeJson = validJsonResult as? NSDictionary {
                        
                        let response = Challenge(jsonDictionary: challengeJson)
                        succesClosure(challenges: response, originalResult: validJsonResult)
                        
                    }
                    else {
                        failureClosure(error: NSError.errorWithLocalizedDescription(localizedDescription: NSLocalizedString("InvalidResponse", comment: "")))
                    }
                }
        }
    }
}
