//
//  Networking.swift
//  Adrian Zghibarta Utils
//
//  Created by Adrian Zghibarta on 1/12/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import Foundation
import Alamofire

/// Class for web services
class Networking: NSObject {
    
    // -------------------------------------------------------------------------------
    // MARK: - Proprietes and constants
    
    // Alamofire manager
    internal var manager: Manager
    // Alamofire manager who has the acces token included in headers
    internal var authManager: Manager
    
    override init() {
        self.manager = Alamofire.Manager()
        self.authManager = Alamofire.Manager()
        super.init()
        
        // Subscribe to notification sender to know when a new auth token was received
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: "changeAuthentificationHeader",
                                                         name: NotificationIdentifiers.AuthTokenDidChangeNotification, object: nil)
        
        // Set the authorisation header at the initialisation if the auth token is saved
        self.changeAuthentificationHeader()
        self.changeLanguageHeader()
    }
    
    /**
     Change the authentification token in the request headers and NSUserDefaults
     */
    func changeAuthentificationHeader() {
        
        print(self)
        var defaultHeaders = Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders ?? [:]
        if let token = UserDefaultStorage.sharedInstance.getToken() {
            defaultHeaders["Authorization"] = "Bearer " + token
        }
        else {
            defaultHeaders["Authorization"] = nil
        }
        
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.HTTPAdditionalHeaders = defaultHeaders
        
        self.authManager = Alamofire.Manager(configuration: configuration)
    }
    
    /**
     This function is used for singleton instances of Networking Manager to force the lazy initialisation
     */
    func forceInit() {
        print("Force Init For reservation manager")
        // This function do nothig but permet to initialisze fo shared instance
    }
    
    /**
     Change the language hader in the requests
     */
    func changeLanguageHeader() {
        
        if let systemLanguage = NSBundle.mainBundle().preferredLocalizations.first {
            var defaultHeaders = Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders ?? [:]
            
            print(systemLanguage)
            defaultHeaders["language"] = (systemLanguage == "fr" ) ? "fr" : "en"
            
            let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
            configuration.HTTPAdditionalHeaders = defaultHeaders
            
            self.authManager = Alamofire.Manager(configuration: configuration)
            self.manager = Alamofire.Manager(configuration: configuration)
        }
    }
}
