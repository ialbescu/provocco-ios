//
//  NetworkingURLS.swift
//  Adrian Zghibarta
//
//  Created by Adrian Zghibarta on 3/29/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import UIKit

/// All url used in this project
class NetworkingURLS: NSObject {
    
    // -------------------------------------------------------------------------------
    // MARK: - Constants
    
    // -------------------------------------------------------------------------------
    // MARK: - Base urls
    static let BASE_URL = "http://10.185.5.130:8585"
    
    //
    //
    // -------------------------------------------------------------------------------
    // MARK: - URLS
    static let LOGIN_URL = BASE_URL + "/register"
    static let CREATE_CHALLENGE = BASE_URL + "/create"
    static let ALL_CHALLENGES_URL = BASE_URL + "/getChallenges"
    static let CHALLANGES = BASE_URL + "/"
}
