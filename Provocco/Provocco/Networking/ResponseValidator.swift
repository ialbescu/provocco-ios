//
//  ResponseValidator.swift
//  Adrian Zghibarta Utils
//
//  Created by Adrian Zghibarta on 1/12/16.
//  Copyright © 2016 Adrian Zghibarta. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

struct ResponseValidator {
    /**
     Return the valid json, else return value is nil and the error handler will be called
     
     - parameter response:       response object
     - parameter failureHandler: handler to process the error
     
     - returns: Valid Response Value
     */
    static func getValidJsonResult(response: Response<AnyObject, NSError>, failureHandler: ((NSError) -> ())?) -> AnyObject? {
        
        // If there is an error then call the error handler
        if let error = response.result.error, let handler = failureHandler {
            handler(error)
        }
        // Else try to get the valid result from response
        else if let responseValue = response.result.value {
            
            // If the response has some errors message in, then handle this errors
            if let error = ResponseValidator.extractErrorFromJsonResponse(responseValue), let handler = failureHandler {
                handler(error)
            }
            else {
                return responseValue
            }
        }
        else if let handler = failureHandler {
            handler(NSError.errorWithLocalizedDescription(localizedDescription: "Resultat inconu"))
        }
        
        return nil
    }
    
    /**
     Check if the JsonResponse is a valid one with succes status, if isn't then return an error
     
     - parameter response: reponse to validate
     
     - returns: NSError if in the Json is an error
     */
    static private func extractErrorFromJsonResponse(response: AnyObject?) -> NSError? {
        
        if let responseToValidate = response {
            
            // Usually the response is an NSDictionary or an NSArray
            if responseToValidate.isKindOfClass(NSDictionary) {
                
                // Cast to dictionary
                let responseAsDictionary = responseToValidate as! NSDictionary
                // Get the error from this dictionary
                return self.extractErrorFromNSDictionary(responseAsDictionary)
            }
            else if responseToValidate.isKindOfClass(NSArray) {
                
                // Cast to array
                let responseAsArray = responseToValidate as! NSArray
                // Get the error from this array
                return self.extractErrorFromNSArray(responseAsArray)
            }
            
            // No error was found ... return nil
            return nil
        }
        else {
            return NSError.errorWithLocalizedDescription(localizedDescription: NSLocalizedString("NullResponseError", comment: ""))
        }
    }
    
    /**
     Get an error from the dictionary response
     
     - parameter dictionary: Dictionary object with info
     
     - returns: Error or nil if no error was found
     */
    static private func extractErrorFromNSDictionary(dictionary: NSDictionary) -> NSError? {
        
        // Check the error_description
        if let errorDescritption = dictionary["error_description"] as? String {
            return NSError.errorWithLocalizedDescription(localizedDescription: errorDescritption)
        }
        
        // Check the server error who return the debug info
        if let _ = dictionary["_debugInfo"] {
            if let errorMessage = dictionary["message"] as? String {
                return NSError.errorWithLocalizedDescription(localizedDescription: errorMessage)
            }
            else {
                return NSError.errorWithLocalizedDescription(localizedDescription: NSLocalizedString("InternalServerError", comment: ""))
            }
        }
        
        // Check the form validation errors
        var errorString: String?
        if let errorsMessage = dictionary["message"] as? String {
            if errorString == nil {
                errorString = ""
            }
            errorString?.appendContentsOf(errorsMessage + " ")
        }
        if let errors = dictionary["errors"] as? NSDictionary {
            if errorString == nil {
                errorString = ""
            }
            for (key, value) in errors {
                errorString?.appendContentsOf("\(key) - \(value) ")
            }
        }
        
        if let errorStringSafe = errorString {
            return NSError.errorWithLocalizedDescription(localizedDescription: errorStringSafe)
        }
        
        return nil
    }
    
    /**
     Get an error from the array response
     
     - parameter array: Array object with info
     
     - returns: Error or nil if no error was found
     */
    static private func extractErrorFromNSArray(array: NSArray) -> NSError? {
        
        // At the moment the server doesn't return an error in array
        // So just return nil
        
        return nil
    }
}
